export class Order {
  // @ts-ignore
  id: number;
  // @ts-ignore
  firstName: string;
  // @ts-ignore
  lastName: string;
  // @ts-ignore
  email: string;
  // @ts-ignore
  location: string;
  // @ts-ignore
  item: string;
  // @ts-ignore
  status: string;
  // @ts-ignore
  payed: boolean;
}
