import { Component, OnInit } from '@angular/core';
import {Order} from "../order";
import {OrderService} from "../order.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-order-list',
  templateUrl: './list-order.component.html',
  styleUrls: ['./list-order.component.css']
})
export class ListOrderComponent implements OnInit {

  // @ts-ignore
  orders: Order[];

  constructor(private orderService: OrderService, private router: Router) { }

  ngOnInit(): void {
    this.getOrders();
  }

  private getOrders(){
    this.orderService.getOrderList().subscribe(
      data => {this.orders = data},
      error => {alert("An error fetching order data has occurred.")}
      );
  }

  detailsOrder(id: number){
    this.router.navigate([`details-order`, id])
  }

  updateOrder(id: number){
    this.router.navigate([`update-order`, id])
  }

  viewLocalOrders(){
    this.router.navigate([`orders-local`])
  }

  deleteOrder(id: number){
    this.orderService.deleteOrder(id).subscribe(
      data => {console.log(data); this.getOrders()},
      error => {alert("An error deleting order data has occurred.")}
    )
  }
}
