import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListOrderComponent} from "./list-order/list-order.component";
import {CreateOrderComponent} from "./create-order/create-order.component";
import {UpdateOrderComponent} from "./update-order/update-order.component";
import {DetailsOrderComponent} from "./details-order/details-order.component";
import {ListOrderLocalComponent} from "./list-order-local/list-order-local.component";

const routes: Routes = [
  //routes to orders
  {path: `orders`, component: ListOrderComponent},
  {path: `orders-local`, component: ListOrderLocalComponent},
  {path: `create-order`, component: CreateOrderComponent},
  {path: `update-order/:id`, component: UpdateOrderComponent},
  {path: `details-order/:id`, component: DetailsOrderComponent},
  {path: ``, redirectTo: `orders`, pathMatch: `full`}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
