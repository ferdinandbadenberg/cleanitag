import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Order} from "./order";

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private baseURL = "http://localhost:8080/api/orders";

  constructor(private httpClient: HttpClient) { }

  getOrderList(): Observable<Order[]>{
    return this.httpClient.get<Order[]>(`${this.baseURL}`);
  }

  getOrderById(id: number): Observable<Order>{
    return this.httpClient.get<Order>(`${this.baseURL}/${id}`);
  }

  createOrder(order: Order): Observable<Order>{
    return this.httpClient.post<Order>(`${this.baseURL}`, order);
  }

  updateOrder(id: number, order: Order): Observable<Object>{
    return this.httpClient.put(`${this.baseURL}/${id}`, order);
  }

  deleteOrder(id: number): Observable<Object>{
    return this.httpClient.delete(`${this.baseURL}/${id}`)
  }
}
