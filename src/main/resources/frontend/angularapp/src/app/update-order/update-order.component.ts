import { Component, OnInit } from '@angular/core';
import {Order} from "../order";
import {OrderService} from "../order.service";
import {Router} from "@angular/router";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-update-order',
  templateUrl: './update-order.component.html',
  styleUrls: ['./update-order.component.css']
})
export class UpdateOrderComponent implements OnInit {

  // @ts-ignore
  id: number;
  order: Order = new Order();
  constructor(private orderService: OrderService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params[`id`];
    this.orderService.getOrderById(this.id).subscribe(
      data => {this.order = data},
      error => alert("An error fetching order data has occurred.")
    )
  }

  goToOrderList(){
    this.router.navigate([`/orders`])
  }

  onSubmit(){
    this.orderService.updateOrder(this.id, this.order).subscribe(
      data => {this.goToOrderList()},
      error => alert("An error updating order data has occurred.")
    );
  }


}
