import { Component, OnInit } from '@angular/core';
import {Order} from "../order";
import {ActivatedRoute, Router} from "@angular/router";
import {OrderService} from "../order.service";

@Component({
  selector: 'app-details-order',
  templateUrl: './details-order.component.html',
  styleUrls: ['./details-order.component.css']
})
export class DetailsOrderComponent implements OnInit {

  // @ts-ignore
  id: number
  // @ts-ignore
  order: Order
  constructor(private orderService: OrderService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params[`id`];
    this.order = new Order();
    this.orderService.getOrderById(this.id).subscribe(
      data => {this.order = data},
      error => {alert("An error fetching order data has occurred.")}
    )
  }

  updateOrder(id: number){
    this.router.navigate([`update-order`, id])
  }
}
