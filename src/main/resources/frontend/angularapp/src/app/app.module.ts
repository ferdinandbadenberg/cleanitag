import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListOrderComponent } from './list-order/list-order.component';
import { HttpClientModule } from "@angular/common/http";
import { CreateOrderComponent } from './create-order/create-order.component';
import {FormsModule} from "@angular/forms";
import { UpdateOrderComponent } from './update-order/update-order.component';
import { DetailsOrderComponent } from './details-order/details-order.component';
import { ListOrderLocalComponent } from './list-order-local/list-order-local.component';

@NgModule({
  declarations: [
    AppComponent,
    ListOrderComponent,
    CreateOrderComponent,
    UpdateOrderComponent,
    DetailsOrderComponent,
    ListOrderLocalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
