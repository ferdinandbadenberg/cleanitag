import { Component, OnInit } from '@angular/core';
import {OrderService} from "../order.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-list-order-local',
  templateUrl: './list-order-local.component.html',
  styleUrls: ['./list-order-local.component.css']
})
export class ListOrderLocalComponent implements OnInit {

  // @ts-ignore
  orders: Order[];
  // Assume fixed local store Basel
  location: string = "Basel";

  constructor(private orderService: OrderService, private router: Router) { }

  ngOnInit(): void {
    this.getOrders();
  }

  private getOrders(){
    this.orderService.getOrderList().subscribe(
      data => {this.orders = data.filter(data => data.location === this.location)},
      error => {alert("An error fetching order data has occurred.")}
    );
  }

  detailsOrder(id: number){
    this.router.navigate([`details-order`, id])
  }

  updateOrder(id: number){
    this.router.navigate([`update-order`, id])
  }

  deleteOrder(id: number){
    this.orderService.deleteOrder(id).subscribe(
      data => {console.log(data); this.getOrders()},
      error => {alert("An error deleting order data has occurred.")}
    )
  }

  viewAllOrders(){
    this.router.navigate([`orders`])
  }
}
