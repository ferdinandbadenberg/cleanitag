import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOrderLocalComponent } from './list-order-local.component';

describe('ListOrderLocalComponent', () => {
  let component: ListOrderLocalComponent;
  let fixture: ComponentFixture<ListOrderLocalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListOrderLocalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOrderLocalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
