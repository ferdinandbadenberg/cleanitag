package ch.cleanit.CleanItDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class CleanItDemoApplication {

	@RequestMapping("/")
	public String greet(){
		return "Welcome to Clean It! AG!";
	}

	public static void main(String[] args) {
		SpringApplication.run(CleanItDemoApplication.class, args);
	}

}
