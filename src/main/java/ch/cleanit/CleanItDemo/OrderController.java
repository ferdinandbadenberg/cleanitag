package ch.cleanit.CleanItDemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/")
public class OrderController {

    @Autowired
    private OrderRepository orderRepository;

    @EventListener
    public void appReady(ApplicationReadyEvent event) {
        //fill with demo data
        orderRepository.save(new Order("Tony", "Stark", "tonystark@avengers.ch", "Basel", "Trousers", "Processing", true));
        orderRepository.save(new Order("Stephen", "Strange", "stephenstrange@avengers.ch", "Zürich", "Cape", "Received",false));
        orderRepository.save(new Order("Bruce", "Banner", "brucebanner@avengers.ch", "Bern", "Shirt", "Closed",true));
    }

    //get all orders
    @GetMapping("/orders")
    public List<Order> getAllOrders(){
        return orderRepository.findAll();
    }

    //get order by id
    @GetMapping("/orders/{id}")
    public ResponseEntity<Order> getOrderById(@PathVariable long id){
        Order order = orderRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("No order with id "+id));
        return ResponseEntity.ok(order);
    }

    //create order
    @PostMapping("/orders")
    public Order createOrder(@RequestBody Order order){
        return orderRepository.save(order);
    }

    //update order by id
    @PutMapping("/orders/{id}")
    public ResponseEntity<Order> updateOrderById(@PathVariable long id, @RequestBody Order newOrder){
        Order order = orderRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("No order with id "+id));

        //update order
        order.setFirstName(newOrder.getFirstName());
        order.setLastName(newOrder.getLastName());
        order.setEmail(newOrder.getEmail());
        order.setLocation(newOrder.getLocation());
        order.setItem(newOrder.getItem());
        order.setStatus(newOrder.getStatus());
        order.setPayed(newOrder.getPayed());

        Order updatedOrder = orderRepository.save(order);
        return ResponseEntity.ok(updatedOrder);
    }

    //delete order by id
    @DeleteMapping("/orders/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteOrder(@PathVariable long id){
        orderRepository.deleteById(id);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted",Boolean.TRUE);
        return ResponseEntity.ok(response);
    }
}
